module lab6_b3(input En, X, Y, K_in, output reg U, K_out);

/*
assign {U, K_out} = X + Y + K_in;
assign {U, K_out} = X - Y - K_in;
*/

always @ (En | X | Y | K_in)
begin
	/*
	if(En)
		{U, K_out} = X + Y + K_in;
	if(~En)
		{U, K_out} = X - Y - K_in;
	*/
	if (En)
		begin
			U = X ^ Y ^ K_in;
			K_out = (X & Y) | (Y & K_in) | (X & K_in);
		end
	else
		begin
			U = X ^ Y ^ K_in;
			K_out = (~X & Y) | (Y & K_in) | (~X & K_in);
		end
end

	
endmodule