`timescale 1 ns/1 ns

module lab6_b2_testbench();

reg En_s, X_s, Y_s, K_in_s;
wire U_s, K_out_s;

lab6_b2 t1(.En(En_s), .X(X_s), .Y(Y_s), .K_in(K_in_s), .U(U_s), .K_out(K_out_s));

initial begin

En_s <= 0; X_s <= 0; Y_s <= 0; K_in_s <= 0;
#10;
En_s <= 0; X_s <= 0; Y_s <= 0; K_in_s <= 1;
#10;
En_s <= 0; X_s <= 0; Y_s <= 1; K_in_s <= 0;
#10;
En_s <= 0; X_s <= 0; Y_s <= 1; K_in_s <= 1;
#10;
En_s <= 0; X_s <= 1; Y_s <= 0; K_in_s <= 0;
#10;
En_s <= 0; X_s <= 1; Y_s <= 0; K_in_s <= 1;
#10;
En_s <= 0; X_s <= 1; Y_s <= 1; K_in_s <= 0;
#10;
En_s <= 0; X_s <= 1; Y_s <= 1; K_in_s <= 1;
#10;
En_s <= 1; X_s <= 0; Y_s <= 0; K_in_s <= 0;
#10;
En_s <= 1; X_s <= 0; Y_s <= 0; K_in_s <= 1;
#10;
En_s <= 1; X_s <= 0; Y_s <= 1; K_in_s <= 0;
#10;
En_s <= 1; X_s <= 0; Y_s <= 1; K_in_s <= 1;
#10;
En_s <= 1; X_s <= 1; Y_s <= 0; K_in_s <= 0;
#10;
En_s <= 1; X_s <= 1; Y_s <= 0; K_in_s <= 1;
#10;
En_s <= 1; X_s <= 1; Y_s <= 1; K_in_s <= 0;
#10;
En_s <= 1; X_s <= 1; Y_s <= 1; K_in_s <= 1;
#10;

end

endmodule