onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /Lab6TestBench/En_s
add wave -noupdate /Lab6TestBench/X_s
add wave -noupdate /Lab6TestBench/Y_s
add wave -noupdate /Lab6TestBench/K_in_s
add wave -noupdate /Lab6TestBench/U_s
add wave -noupdate /Lab6TestBench/K_out_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {256 ns}
