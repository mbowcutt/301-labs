module lab6_b1(input En, X, Y, K_in, output U, K_out);

wire z, z1, z2, z3, z4, z5;

xnor xnor0(z, En, X);

and and0(z1, z, Y);

and and1(z3, Y, K_in);

and and2(z4, z, K_in);

xor xor0(z5, X, Y);

xor xor1(U, K_in, z5);

or or0(K_out, z1, z2);

or or1(z2, z3, z4);

endmodule
