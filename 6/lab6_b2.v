module lab6_b2(input En, X, Y, K_in, output U, K_out);

assign z = En ^~ X;

assign z1 = z & Y;

assign z2 = z3 | z4;

assign z3 = Y & K_in;

assign z4 = K_in & z;

assign z5 = X ^ Y;

assign K_out = z1 | z2;

assign U = z5 ^ K_in;

endmodule