`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Case Western Reserve University
// Engineer: Matt McConnell
// 
// Create Date:    23:54:00 02/04/2017 
// Project Name:   EECS301 Digital Design
// Design Name:    Lab #5 Project
// Module Name:    PS2_Keyboard_Keycode_Parser
// Target Devices: Altera Cyclone V
// Tool versions:  Quartus v15.0
// Description:    PS/2 Keyboard Keycode Parser
//                 This module will process the raw keyboard data to report
//                 full key presses.
//                 
// Dependencies:   
//
//////////////////////////////////////////////////////////////////////////////////

module PS2_Keyboard_Keycode_Parser
(
	// PS/2 Key Code Input Signals
	input            KEY_READY,
	input      [7:0] KEY_DATA,
	
	// Key Pressed Status Signals
	output reg       KEY_PRESSED_MAKE,
	output reg       KEY_PRESSED_BREAK,
	output reg [7:0] KEY_PRESSED_DATA,
	
	// System Signals
	input CLK,
	input RESET
);
	
	reg [5:0] State;
	localparam [5:0]
		S0 = 6'b000001,
		S1 = 6'b000010,
		S2 = 6'b000100,
		S3 = 6'b001000,
		S4 = 6'b010000,
		S5 = 6'b100000;
	
	// Key Modifier Status Flags
	reg key_release_flag;  // Release Key Code Received (0xF0)
//	reg key_extended_flag;  // Extended Key Code Received (0xE0)
	
	
	//
	// Check Key Code for Modifier Status Flags
	//   Note: This removes the 8-bit wide mux propagation
	//         delay from the state machine timing.
	//
	reg [1:0] status_flag;
	
	always @(posedge CLK)
	begin
		case (KEY_DATA)
			8'hE0   : status_flag <= 2'h1;
			8'hF0   : status_flag <= 2'h2;
			default : status_flag <= 2'h0;
		endcase
	end
	
	
	//
	// PS/2 Serial Controller State Machine
	//

	always @(posedge CLK)
	begin
	
		// reset logic
		if (RESET)
		begin
			State <= S0;
			KEY_PRESSED_MAKE <= 1'b0;
			KEY_PRESSED_BREAK <= 1'b0;
			KEY_PRESSED_DATA <= 8'h00;
			key_release_flag <= 1'b0;
		end
		
		// state logic
		else
		begin
			case (State)
			
				// wait for key ready
				S0:
				begin
					if (KEY_READY)
					begin
						State <= S1;
					end
				end
				
				// set release status flag
				S1:
				begin
					if (status_flag == 2'h1)
					begin
						key_release_flag <= 1'b0;
					end
					else if (status_flag == 2'h2)
					begin
						key_release_flag <= 1'b1;
					end
					
					if (status_flag != 2'h0)
					begin
						State <= S0;
					end
					else
					begin
						State <= S2;
					end
				end
				
				// output key press data
				S2:
				begin
					KEY_PRESSED_DATA <= KEY_DATA;
					if (key_release_flag)
					begin
						State <= S4;
					end
					else
					begin
						State <= S3;
					end
				end
				
				// assert key pressed
				// clear release status
				S3:
				begin
					KEY_PRESSED_MAKE <= 1;
					key_release_flag <= 1'b0;
					State <= S5;
				end

				// assert key released
				// clear release status				
				S4:
				begin
					KEY_PRESSED_BREAK <= 1;
					key_release_flag <= 1'b0;
					State <= S5;
				end
				
				// clear control signals
				S5:
				begin
					KEY_PRESSED_MAKE <= 1'b0;
					KEY_PRESSED_BREAK <= 1'b0;
					//KEY_PRESSED_DATA <= 8'h00;
					//key_release_flag <= 1'b0;
					State <= S0;
				end
				
			endcase
		end
	end
	
endmodule
