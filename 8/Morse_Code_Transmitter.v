`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Case Western Reserve University
// Engineer: Matt McConnell
// 
// Create Date:    22:23:00 01/17/2017 
// Project Name:   EECS301 Digital Design
// Design Name:    Lab #5 Project
// Module Name:    Morse_Code_Transmitter
// Target Devices: Altera Cyclone V
// Tool versions:  Quartus v17.0
// Description:    Morse Code Transmitter
//                 
// Dependencies:   
//
//////////////////////////////////////////////////////////////////////////////////

module Morse_Code_Transmitter
(
	// Input Signals
	input        SEND,
	input [12:0] SEQ_DATA,
	input  [3:0] SEQ_LEN,
	input        RATE_TICK,
	
	// Output Signals
	output reg  DONE,
	output reg  MC_OUT,
	
	// System Signals
	input 		CLK,
	input 		RESET
);

	//
	// Transmitter State Machine
	//
	reg  [4:0] bit_count_reg;
	reg [12:0] bit_shift_reg;
	
	wire       bit_count_done = bit_count_reg[4];
	
	// !! Lab 5: Add the Morse Code Transmitter State Machine here !!
	
	reg[5:0] State;
	localparam [5:0]
		S0 = 6'b000001,
		S1 = 6'b000010,
		S2 = 6'b000100,
		S3 = 6'b001000,
		S4 = 6'b010000,
		S5 = 6'b100000;
		
	initial begin
		State <= S0;
	end
	
	always @ (posedge CLK)
	begin
	
		if (RESET)
		begin
			State <= S0;
			DONE <= 1'b0;
			MC_OUT <= 1'b0;
			bit_shift_reg <= 13'h0000;
			bit_count_reg <= 5'h00;
		end
		
		else
		begin
			case(State)
				S0:
				begin
					DONE <= 0;
					if(SEND)
						State <= S1;
				end
				
				S1:
				begin
					bit_count_reg <= {1'b0, ~SEQ_LEN};
					bit_shift_reg <= SEQ_DATA;
					State <= S2;
				end
				
				S2:
				begin
					if (RATE_TICK)
						State <= S3;
				end
				
				S3:
				begin
					MC_OUT <= bit_shift_reg[0];
					bit_count_reg = bit_count_reg + 1;
					State <= S4;
				end
				
				S4:
				begin
					bit_shift_reg = {1'b0, bit_shift_reg[12:1]};
					if(bit_count_reg == bit_count_done)
						State <= S5;
					else
						State <= S2;
				end
				
				S5:
				begin
					DONE <= 1;
					State <= S0;
				end
				
			endcase
		end
	
	end

endmodule
