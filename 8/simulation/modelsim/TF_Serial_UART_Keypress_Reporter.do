onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/CLK
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/RESET
add wave -noupdate -divider {Control Signals}
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/TX_SEND
add wave -noupdate -radix hexadecimal /TF_Serial_UART_Keypress_Reporter/TX_DATA
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/TX_DONE
add wave -noupdate -divider {UART TX Bus}
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/UART_TX
add wave -noupdate -divider {Transmitter FSM}
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/uut/uart_tx/State
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/uut/uart_tx/BAUD_TICK
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/uut/uart_tx/tx_frame_data_reg
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/uut/uart_tx/tx_bit_counter
add wave -noupdate /TF_Serial_UART_Keypress_Reporter/uut/uart_tx/tx_bit_counter_done
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {120433064 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {120550331 ps}
run 120 us
