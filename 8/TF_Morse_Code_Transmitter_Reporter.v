`timescale 1ns / 1ps

module TF_Morse_Code_Transmitter_Reporter();


	//
	// System Clock Emulation
	//
	localparam CLK_RATE_HZ = 500000000; // 500 MHz
	localparam CLK_HALF_PER = ((1.0 / CLK_RATE_HZ) * 1000000000.0) / 2.0; // ns
	
	reg        CLK;
	
	initial
	begin
		CLK = 1'b0;
		forever #(CLK_HALF_PER) CLK = ~CLK;
	end


	//
	// Unit Under Test: Morse_Code_Transmitter
	//
	reg 			RESET;
	reg        	SEND;
	reg [12:0]	SEQ_DATA;
	reg  [3:0] 	SEQ_LEN;
	reg        	RATE_TICK;
	
	// Output Signals
	wire  DONE;
	wire  MC_OUT;
	
	Morse_Code_Transmitter m
	(
		// Morse Code Transmitter Inputs
		.SEND( SEND ),
		.SEQ_DATA( SEQ_DATA ),
		.SEQ_LEN( SEQ_LEN ),
		.RATE_TICK( RATE_TICK ),

		// Morse Code Transmitter Outputs
		.DONE( DONE ),
		.MC_OUT( MC_OUT ),
		
		// System Signals
		.CLK( CLK ),
		.RESET( RESET )
	);


	//
	// Test Sequence
	//
	initial
	begin
		
		// Initialize Signals
		RESET = 1'b1;

		SEND = 1'b0;
		SEQ_DATA = 13'h0000;
		SEQ_LEN=4'h0;
		RATE_TICK = 0;
		
		#500;
		
		// Release Reset
		RESET = 1'b0;
		#500;
		
		// Send Data
		@(posedge CLK);
		SEND = 1;
		SEQ_DATA = 11'b10111010111;
		SEQ_LEN = 11;
		
		#500;
		
		@(posedge CLK);
		SEND = 0;
		SEQ_DATA=13'h0000;
		SEQ_LEN=4'h0;
		
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		RATE_TICK = ~RATE_TICK;
		#500;
		$stop;
	end
	
endmodule
