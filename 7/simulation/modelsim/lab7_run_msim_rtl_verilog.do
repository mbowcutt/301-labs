transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+H:/My\ Documents/301-labs/7 {H:/My Documents/301-labs/7/arbiter.v}
vlog -vlog01compat -work work +incdir+H:/My\ Documents/301-labs/7 {H:/My Documents/301-labs/7/sync.v}
vlog -vlog01compat -work work +incdir+H:/My\ Documents/301-labs/7 {H:/My Documents/301-labs/7/safety.v}
vlog -vlog01compat -work work +incdir+H:/My\ Documents/301-labs/7 {H:/My Documents/301-labs/7/lab7_C_toplevel.v}

vlog -vlog01compat -work work +incdir+H:/My\ Documents/301-labs/7 {H:/My Documents/301-labs/7/lab7_C_testbench.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cyclonev_ver -L cyclonev_hssi_ver -L cyclonev_pcie_hip_ver -L rtl_work -L work -voptargs="+acc"  lab7_C_testbench

add wave *
view structure
view signals
run -all
