`timescale 1 ns/1 ns

module safety_testbench();

reg GT1, GT2, GT3;
wire GS1, GS2, GS3, ERROR;

safety safe
(
	.i1(GT1),
	.i2(GT2),
	.i3(GT3),
	.gs1(GS1),
	.gs2(GS2),
	.gs3(GS3),
	.err(ERROR)
);

initial begin

GT1 = 0;
GT2 = 0;
GT3 = 0;
#5;

GT1 = 1;
#5;
GT2 = 1; // Error
#5; 
GT1 = 0;
#5;
GT3 = 1; // Error
#5;
GT2 = 0;
#5;
GT1 = 1; // Error
#5;
GT3 = 0;
#5;
GT2 = 1;
GT3 = 1; // Error
#5;
end

endmodule