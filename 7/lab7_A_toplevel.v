module lab7_A_toplevel
(
	input [9:0] SW,
	output [9:0] LED,
	
	input CLOCK_50
);

arbiter a1
(
	.rq1(SW[9]),
	.rq2(SW[8]),
	.rq3(SW[7]),
	.RESET(SW[0]),
	.CLK(CLOCK_50),
	.gt1(LED[2]),
	.gt2(LED[1]),
	.gt3(LED[0])
);

endmodule