module lab7_C_toplevel
(
	input [9:0] SW,
	output [9:0] LED,
	
	input CLOCK_50
);

wire [2:0] w;
wire [2:0] s;

sync synchronizer
(
	.d1(SW[9]),
	.d2(SW[8]),
	.d3(SW[7]),
	.clk(CLOCK_50),
	.q1(w[0]),
	.q2(w[1]),
	.q3(w[2])
);

arbiter a1
(
	.rq1(w[0]),
	.rq2(w[1]),
	.rq3(w[2]),
	.RESET(SW[0]),
	.CLK(CLOCK_50),
	.gt1(s[0]),
	.gt2(s[1]),
	.gt3(s[2])
);

safety safe
(
	.i1(s[0]),
	.i2(s[1]),
	.i3(s[2]),
	.gs1(LED[2]),
	.gs2(LED[1]),
	.gs3(LED[0]),
	.err(LED[9])
);

endmodule