`timescale 1 ns/1 ns

module lab7_C_testbench();

reg RQ1, RQ2, RQ3, RESET, CLOCK;
wire GS1, GS2, GS3, ERR;

wire [2:0] w;
wire [2:0] s;

sync synchronizer(
	.d1(RQ1),
	.d2(RQ2),
	.d3(RQ3),
	.clk(CLOCK),
	.q1(w[0]),
	.q2(w[1]),
	.q3(w[2])
);

arbiter test(
	.rq1(w[0]),
	.rq2(w[1]),
	.rq3(w[2]),
	.RESET(RESET),
	.CLK(CLOCK),
	.gt1(s[0]),
	.gt2(s[1]),
	.gt3(s[2])
);


safety safe
(
	.i1(s[0]),
	.i2(s[1]),
	.i3(s[2]),
	.gs1(GS1),
	.gs2(GS2),
	.gs3(GS3),
	.err(ERR)
);

always 
	#5 CLOCK = !CLOCK;

initial begin

// Initialize clock and reset
CLOCK = 0;
RESET = 0;
#2;

//
// TEST STATE TRANSITIONS
//

//
// Initialize to Idle State
//
RQ1 = 0;
RQ2 = 0;
RQ3 = 0;
#10;

// Transition from Idle to GT1
RQ1 = 1;
#10;

// Transition from GT1 to Idle
RQ1 = 0;
#10;

// Transition from Idle to GT2
RQ2 = 1;
#10;

// Transition from GT2 to Idle
RQ2 = 0;
#10;

// Transition from Idle to GT3
RQ3 = 1;
#10;

// Transition from GT3 to Idle
RQ3 = 0;
#10;

//
// Initialize to GT1
//
RQ1 = 1;
#10;

// Transition from GT1 to GT2
RQ1 = 0;
RQ2 = 1;
#10;

// Transition from GT2 to GT1
RQ1 = 1;
RQ2 = 0;
#10;

// Transition from GT1 to GT3
RQ1 = 0;
RQ3 = 1;
#10;

// Transition from GT3 to GT1
RQ1 = 1;
RQ3 = 0;
#10;

//
// Initialize to GT2
//
RQ1 = 0;
RQ2 = 1;
#10;

// Transition from GT2 to GT3
RQ2 = 0;
RQ3 = 1;
#10;

// Transition from GT3 to GT2
RQ2 = 1;
RQ3 = 0;
#10;

//
// Test Request Priority and RESET
//

// Initialze to GT3
RQ2 = 0;
RQ3 = 1;
#10;

// GT2 has priority over GT3
RQ2 = 1;
#10;

// Test RESET, then GT1 has priority over GT3
RESET = 1;
#1;
RESET = 0;
RQ1 = 1;
RQ2 = 0;
#9;

// Test RESET, then GT1 has priority over GT2 and GT3
RESET = 1;
#1;
RESET = 0;
RQ2 = 1;
#9;

$stop;

end


endmodule