# EECS 301 Lab 7

* Michael Bowcutt <mwb71@case.edu>
* Ryan Xie <rlx3@case.edu>

# Arbiter State Machine

Files:

* lab7.qpf
* pinout.csv
* report.pdf

## Procedure A

Files:

* arbiter.v
* arbiter_RTL.PNG
* lab7_A_toplevel.v
* lab7_A_testbench.v
* ProcedureA_waveform.PNG

## Procedure B

Files:

* sync.v
* sync_RTL.PNG
* lab7_B_toplevel.v
* lab7_B_testbench.v
* ProcedureB_waveform.PNG

## Procedure C

Files:

* safety.v
* safety_RTL.PNG
* safety_testbench_waveform.PNG
* lab7_C_toplevel.v
* lab7_C_testbench.v
* lab7_C_toplevel_RTL.PNG
* ProcedureC_waveform.PNG

