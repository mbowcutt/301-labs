module safety
(
	input i1, i2, i3,
	output gs1, gs2, gs3, err
);


assign gs1 = i1 & ~(i2 | i3);
assign gs2 = i2 & ~(i1 | i3);
assign gs3 = i3 & ~(i1 | i2);
assign err = (i1 & i2) | (i2 & i3) | (i1 & i3);

endmodule
