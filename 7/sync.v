module sync
(
	input d1, d2, d3,
	output reg q1, q2, q3,
	
	input clk
);

always @(posedge clk)
begin
	
	q1 = d1;
	q2 = d2;
	q3 = d3;

end

endmodule