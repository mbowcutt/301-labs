module arbiter
(
	// Control Signals
	input 		rq1, rq2, rq3,
	output reg 	gt1, gt2, gt3,
	
	// System Signals
	input CLK, RESET
);

	// State Register
	reg[3:0] State;
	localparam [3:0]
		IDLE = 4'b0001,
		GT1 = 4'b0010,
		GT2 = 4'b0100,
		GT3 = 4'b1000;
	
	initial begin
		State = IDLE;
	end
	
	// Handle clock or reset signal
	always @(posedge CLK, posedge RESET)
	begin

	
		// Async Reset
		if (RESET)
		begin
			State <= IDLE;
			gt1 = 0;
			gt2 = 0;
			gt3 = 0;
		end
		
		else
		begin
		
			// State Transitions
			case (State) 
				
				IDLE: // Idle State
				begin
				
					if(rq1)
					begin
						State <= GT1;
						gt1 = 1;
						gt2 = 0;
						gt3 = 0;
					end
					
					else if(~rq1 & rq2)
					begin
						State <= GT2;
						gt1 = 0;
						gt2 = 1;
						gt3 = 0;
					end
					
					else if(~rq1 & ~rq2 & rq3)
					begin
						State <= GT3;
						gt1 = 0;
						gt2 = 0;
						gt3 = 1;
					end
					
					else // default IDLE
					begin
						gt1 = 0;
						gt2 = 0;
						gt3 = 0;
					end
				
				end
			
				GT1:
				begin
					
					// Switch to appropriate state
					if (~rq1 & ~rq2 & ~rq3)
					begin
						State <= IDLE;
						gt1 = 0;
						gt2 = 0;
						gt3 = 0;
					end
					
					else if (~rq1 & rq2)
					begin
						State <= GT2;
						gt1 = 0;
						gt2 = 1;
						gt3 = 0;
					end
					
					else if (~rq1 & ~rq2 & rq3)
					begin
						State <= GT3;
						gt1 = 0;
						gt2 = 0;
						gt3 = 1;
					end
					
					else // default GT1
					begin
						gt1 = 1;
						gt2 = 0;
						gt3 = 0;
					end
					
				end				
				
				GT2:
				begin
				
					// Switch to appropriate state
					if (~rq1 & ~rq2 & ~rq3)
					begin
						State <= IDLE;
						gt1 = 0;
						gt2 = 0;
						gt3 = 0;
					end
					
					if (rq1)
					begin
						State <= GT1;
						gt1 = 1;
						gt2 = 0;
						gt3 = 0;
					end
					
					if (~rq1 & ~rq2 & rq3)
					begin
						State <= GT3;
						gt1 = 0;
						gt2 = 0;
						gt3 = 1;
					end
					
					else // default GT2
					begin
						gt1 = 0;
						gt2 = 1;
						gt3 = 0;
					end
				
				end
				
				GT3:
				begin
				
					// switch to appropriate state
					if (~rq1 & ~rq2 & ~rq3)
					begin
						State <= IDLE;
						gt1 = 0;
						gt2 = 0;
						gt3 = 0;
					end
					
					else if (rq1)
					begin
						State <= GT1;
						gt1 = 1;
						gt2 = 0;
						gt3 = 0;
					end
					
					else if (~rq1 & rq2)
					begin
						State <= GT2;
						gt1 = 0;
						gt2 = 1;
						gt3 = 0;
					end
					
					else // default GT3
					begin
						gt1 = 0;
						gt2 = 0;
						gt3 = 1;
					end
					
				end
				
			endcase
			
		end
		/*
		begin
		
			// Output Assignments
			case (State)
				
				IDLE:
				begin				// Assign idle output
					gt1 = 0;
					gt2 = 0;
					gt3 = 0;
				end

				GT1:
				begin				// grant IRQ 1
					gt1 = 1;
					gt2 = 0;
					gt3 = 0;
				end
					
				GT2:
				begin				// grant IRQ 2
					gt1 = 0;
					gt2 = 1;
					gt3 = 0;
				end
					
				GT3:
				begin				// grant IRQ 3
					gt1 = 0;
					gt2 = 0;
					gt3 = 1;
				end
					
			endcase
		end
		*/
	end
	/*
	always @(posedge CLK, posedge RESET)
	begin
		
		
	end
	*/
endmodule